# Chat App

Node.js, React (HTML/CSS, JavaScript)<br><br>
Chat käyttöliittymä on tehty Reactilla. Chatin serveri on JavaScriptillä tehty WebSocket server.

## Ominaisuudet
- Käyttäjä voi päättää nimimerkin ja nimensä värin
- Sen jälkeen käyttäjä voi lähettää viestejä chatissa
- Ja vastaanottaa muiden käyttäjien viestejä
