import { useState, createContext } from "react"
import Search from "./Search.jsx";
import WeatherInfo from "./WeatherInfo.jsx";
import Footer from "./Footer.jsx";

const WeatherContext = createContext();

function App() {
  const [currentWeather, setCurrentWeather] = useState(null);

  return (
    <div id="weather-app">
      <WeatherContext.Provider value={[currentWeather, setCurrentWeather]}>
        <Search />
        <WeatherInfo />
      </WeatherContext.Provider>
      <Footer />
    </div>
  );
}

export default App;
export { WeatherContext };
