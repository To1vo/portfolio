import { useState } from "react";

function useFetchWeather(){
    const [error, setError] = useState(false);
    const [data, setData] = useState(null);

    const url = import.meta.env.VITE_API_URL;
    const key = import.meta.env.VITE_API_KEY;

    //Haetaan tietyn kaupungin säätiedot
    const fetcher = async (city) => {
        try {
            const result = await fetch(`${url}${city}&appid=${key}`);
            //Käsitellään response status
            if(!result.ok){
                setData(null);
                setError(true);
                return;
            }
            const data = await result.json();
            setData(data);
        } catch(err){
            console.log("Fetch failed!");
            console.error(err);
            setError(true);
        }
    }

    return { data, error, setError, fetcher }
}

export default useFetchWeather;
