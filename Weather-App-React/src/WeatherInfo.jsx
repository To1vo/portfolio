import { useContext } from "react";
import { WeatherContext } from "./App.jsx";
import clearImg from "./assets/clear.png";
import cloudsImg from "./assets/clouds.png";
import drizzleImg from "./assets/drizzle.png";
import mistImg from "./assets/mist.png";
import rainImg from "./assets/rain.png";
import snowImg from "./assets/snow.png";
import windImg from "./assets/wind.png";
import humidityImg from "./assets/humidity.png";
import cloudImg from "./assets/cloud.png";
import locationImg from "./assets/location.png";

function WeatherInfo(){
    const [currentWeather] = useContext(WeatherContext);

    //Säätiedot näkyville
    let weatherImg;
    if(currentWeather !== null){
        switch(currentWeather.weather[0].main){
        case "Rain":
            weatherImg = rainImg;
            break;
        case "Clear":
            weatherImg = clearImg;
            break;
        case "Clouds":
            weatherImg = cloudsImg;
            break;
        case "Drizzle":
            weatherImg = drizzleImg;
            break;
        case "Mist":
            weatherImg = mistImg;
            break;
        case "Snow":
            weatherImg = snowImg;
            break;
        }
    } else {
        return null;
    }

    return (
        <div className="weather-info">
            <h1>{Math.round(currentWeather.main.temp)}°C</h1>
            <div className="location">
                <img src={locationImg} alt="location" />
                <p>{currentWeather.name}</p>
            </div>
            <p id="weather-type">{currentWeather.weather[0].main}</p>
            <img id="weather-img" src={weatherImg} alt="weather-img" />
            <div className="weather-more-info">
                <div>
                    <img src={cloudImg} alt="cloud" />
                    <p>{currentWeather.clouds.all}%</p>
                </div>
                <div>
                    <img src={windImg} alt="wind" />
                    <p>{currentWeather.wind.speed}m/s</p>
                </div>
                <div>
                    <img src={humidityImg} alt="humidity" />
                    <p>{currentWeather.main.humidity}%</p>
                </div>
            </div>
        </div>
    );
}

export default WeatherInfo;
