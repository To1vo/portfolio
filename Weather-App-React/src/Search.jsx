import { useRef, useContext, useEffect } from "react";
import useFetchWeather from "./useFetchWeather.js";
import { WeatherContext } from "./App.jsx";

function Search(){
    const [, setCurrentWeather] = useContext(WeatherContext);
    const { data: weatherData, error, setError, fetcher} = useFetchWeather();
    const inputRef = useRef(null);
    
    useEffect(() => {
      const handleKeyPress = (e) => {
        if(e.code === "Enter") handleSearch();
      }
      window.addEventListener("keydown", handleKeyPress);
      
      return () => window.removeEventListener("keydown", handleKeyPress);
    }, []);

    //Käsitellään data
    useEffect(() => {
      setCurrentWeather(weatherData);
    }, [weatherData]);
    
    const handleSearch = () => {
      const inputValue = inputRef.current.value;
      if(inputValue.trim() != "") fetcher(inputValue);
    }

    const clearErrorOnInput = () => {
      setError(false);
    }

    return (
      <>
        <div className="input-container">
          <input 
            onInput={clearErrorOnInput} 
            ref={inputRef} 
            type="text" 
            spellCheck="false" 
            placeholder="City name..."
          />
          <button onClick={handleSearch}>Search</button>
        </div>
        {error && <p id="error-text">Error occured, probably invalid city name...</p>}
      </>
    );
}

export default Search;
