# Weather App React

React (HTML/CSS, JavaScript)

Ohjelma hakee säätiedot [OpenWeatherMap API:sta](https://openweathermap.org/api)

## Ominaisuudet
- Käyttäjä voi hakea tietyn kaupungin/maan säätietoja
- Ohjelma tarkastaa ja ilmoittaa virheistä
- Ohjelma näyttää:
    - Säähän sopivan kuvan
    - Lämpötilan
    - Tuulen nopeuden
    - Kosteuden
    - Pilvisyyden
