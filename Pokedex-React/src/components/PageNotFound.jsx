function PageNotFound(){
    return (
        <div id="page-not-found">
            <h2>Sorry! Page Not Found</h2>
            <p>The page you are looking for is not here</p>
            <a href="/">Go to Pokédex</a>
        </div>
    );
}

export default PageNotFound;