# Pokedex React App

React (HTML/CSS, JavaScript)<br>
[Demo](https://pokedex-to1vo.vercel.app/)

Ohjelma hakee pokemonien tiedot [PokeAPI:sta](https://pokeapi.co/).<br>
Käyttäjä voi tarkastella eri pokemonien tietoja,<br>
Lajitella pokemonit tiettyyn järjestykseen,<br>
Hakea tiettyä pokemonia nimellä<br>

Ohjelma sisältää myös itse tehdyn BrowserRouter systeemin, joten tietyn pokemonin haku onnistuu myös osoiteriviltä.
