// Toivo Lindholm 2023
// Kappaleen, artistin ja albumin haku 
import { token } from "./auth.js";
import { clearDisplay } from "./main.js";
import { mainDiv, showArtist } from "./artist.js";
import { getTrackDurationMin } from "./track.js";

// Html elements
const searchMain = document.querySelector("[data-search]");
export const searchInput = document.querySelector("[data-search-input]");
const searchInfo = document.querySelector("[data-search-info]");
const inputClear = document.querySelector("[data-input-clear]");
// Tracks
const searchBestTrackResult = document.querySelector("[data-search-best-track-result]");
const searchTrackResults = document.querySelector("[data-search-track-results]");
const searchTrackResultsInner = document.querySelector("[data-search-track-results-inner]");
// Artists
const searchArtistResults = document.querySelector("[data-search-artist-results]");
const searchArtistResultsInner = document.querySelector("[data-artist-results]");
// Albums
const searchAlbumResults = document.querySelector("[data-search-album-results]");
const searchAlbumResultsInner = document.querySelector("[data-search-album-results-inner]");


// Haku näkyville
export function showSearch(){
    clearDisplay();
    displaySearchResultsHtml(false);
    searchInput.value = "";
    mainDiv.style = "";
    searchMain.classList.add("active");
    showSearchInfo(1);
}

// Tarkistetaan haku
function checkSearch(inputValue){
    let searchValue = inputValue.trim();
    //Jos hakusana ei ole tyhjä
    if(searchValue != ""){
        searchInfo.classList.remove("show");
        search(searchValue);
    }
    if(searchInput.value.trim() == ""){
        showSearchInfo(1);
        displaySearchResultsHtml(false);
    }
}

// Suoritetaan haku
async function search(searchValue){
    try {
        const result = await fetch(`https://api.spotify.com/v1/search?q=${searchValue}&type=album,track,artist&limit=4`, {
            headers: {
                "Authorization": "Bearer " + token
            }
        });
        const searchData = await result.json();

        // Data näkyviin
        if(searchData.tracks.items.length > 0){
            showBestTrackResult(searchData.tracks.items[0]);
            showTrackResults(searchData.tracks.items);
        };
        if(searchData.artists.items.length > 0) showArtistResults(searchData.artists.items);
        if(searchData.albums.items.length > 0) showAlbumResults(searchData.albums.items);
    } catch(error){
        //Jos haussa tapahtuu joku virhe
        displaySearchResultsHtml(false);
        showSearchInfo(2);
    }
}
    
const showSearchInfo = (infoCase) => {
    searchInfo.classList.add("show");
    if(infoCase == 1){
        searchInfo.textContent = "Hae kappaletta, artistia tai albumia...";
    } else if(infoCase == 2){
        searchInfo.textContent = "Hakutuloksia ei ole tai haussa tapahtui virhe...";
    }
}

// Parhain kappale tulos näkyviin
const showBestTrackResult = (result) => {
    let bestTrackResult = result;
    let imageSrc = ""
    let artistId = "";
    // Jos kuvaa ei ole
    if(bestTrackResult.album.images.length == 0){
        imageSrc = "img/no-image.png";
    } else {
        imageSrc = bestTrackResult.album.images[1].url;
    }
    let bestTrackResultHtml = `
    <h2>Paras tulos</h2>
    <div class="best-track-result-inner" onclick="_mainJs.showTrack('${bestTrackResult.id}')">
    <img src="${imageSrc}" alt="best-track-result-img">
    <h1 id="best-track-name">${bestTrackResult.name}</h1>
    <p id="best-result-artist-link" class="artist-link">${bestTrackResult.artists[0].name}</p>
    </div>
    `;
    searchBestTrackResult.innerHTML = bestTrackResultHtml;
    artistId = bestTrackResult.artists[0].id;
    document.querySelector("#best-result-artist-link").addEventListener("click", (e) => {
        e.cancelBubble = true;
        showArtist(artistId);
    });
};
// Kappale tulokset näkyviin
const showTrackResults = (result) => {
    searchTrackResultsInner.innerHTML = "";
    let trackResults = result;
    trackResults.forEach((track) => {
        let imageSrc = ""
        // Jos kuvaa ei ole
        if(track.album.images.length == 0){
            imageSrc = "img/no-image.png";
        } else {
            imageSrc = track.album.images[2].url;
        }
        let trackDuration = (((track.duration_ms/1000)/60).toFixed(2)).toString().split(".");
        trackDuration = getTrackDurationMin(trackDuration);
        let trackResultHtml = `
        <div class="track-result">
        <div class="track-result-left">
        <img src="${imageSrc}" alt="track-result-img">
        <div class="track-result-name-and-artist">
        <p class="track-name" onclick="_mainJs.showTrack('${track.id}')">${track.name}</p>
        <p class="artist-link" onclick="_mainJs.showArtist('${track.artists[0].id}')">${track.artists[0].name}</p>
        </div>
        </div>
        <p>${trackDuration}</p>
        </div>
        `;
        searchTrackResultsInner.innerHTML += trackResultHtml;
    });
    searchTrackResults.classList.add("show");
};
// Artisti tulokset näkyviin
const showArtistResults = (result) => {
    searchArtistResultsInner.innerHTML = "";
    let artistResults = result;
    artistResults.forEach((artist) => {
        let imageSrc = ""
        // Jos kuvaa ei ole
        if(artist.images.length == 0){
            imageSrc = "img/no-image.png";
        } else {
            imageSrc = artist.images[1].url;
        }
        let artistResultHtml = `
        <div class="search-artist-result" onclick="_mainJs.showArtist('${artist.id}')">
        <img src="${imageSrc}" alt="artist-result-img">
        <p class="search-result-artist-name">${artist.name}</p>
        <p>Artisti</p>
        </div>
        `;
        searchArtistResultsInner.innerHTML += artistResultHtml;
    });
    searchArtistResults.classList.add("show");
};
// Albumi tulokset näkyviin
const showAlbumResults = (result) => {
    searchAlbumResultsInner.innerHTML = "";
    let albumResults = result;
    albumResults.forEach((album) => {
        let imageSrc = ""
        // Jos kuvaa ei ole 
        if(album.images.length == 0){
            imageSrc = "img/no-image.png";
        } else {
            imageSrc = album.images[1].url;
        }
        let releaseDate = album.release_date.split("-")[0];
        let albumResultHtml = `
        <div class="search-album-result" onclick="_mainJs.showAlbum('${album.id}')">
        <img src="${imageSrc}" alt="artist-result-img">
        <p class="search-result-album-name">${album.name}</p>
        <p>${releaseDate} •  ${album.artists[0].name}</p>
        </div>
        `;
        searchAlbumResultsInner.innerHTML += albumResultHtml;
    });
    searchAlbumResults.classList.add("show");
};

const displaySearchResultsHtml = (a) => {
    if(a == true){
        searchTrackResults.classList.add("show");
        searchArtistResults.classList.add("show");
        searchAlbumResults.classList.add("show");
    } else {
        searchTrackResults.classList.remove("show");
        searchArtistResults.classList.remove("show");
        searchAlbumResults.classList.remove("show");
    }
}

searchInput.addEventListener("input", () => {
    setTimeout(() => {
        checkSearch(searchInput.value)
    }, 250);
});
inputClear.addEventListener("click", () => searchInput.value = "");