// Toivo Lindholm 2023
// Artistin tiedot näkyville
import { clearDisplay, getArtist, getArtistPopularTracks } from "./main.js";
import { getAverageRgbOfImg } from "./avgRgb.js"
import { getTrackDurationMin } from "./track.js";

const artistMain = document.querySelector("[data-artist]");
const artistTop = document.querySelector("[data-artist-top]");
const artistPopularTracks = document.querySelector("[data-artist-popular-tracks]");
const artistPopularTracksInner = document.querySelector("[data-artist-popular-tracks-inner]");
const showMoreTracks = document.querySelector("[data-show-more-tracks]");
export const mainDiv = document.querySelector(".main");

// Katsooko käyttäjä kaikkia kymmentä kappaletta
showMoreTracks.addEventListener("click", () => {
    artistPopularTracksInner.classList.toggle("showAll");
    if(artistPopularTracksInner.classList.contains("showAll")){
        showMoreTracks.textContent = "Näytä vähemmän"
    } else {
        showMoreTracks.textContent = "Näytä lisää"
    }
});

export async function showArtist(artistId){
    if(artistPopularTracksInner.classList.contains("showAll")){
        artistPopularTracksInner.classList.remove("showAll");
    }
    displayArtist(await getArtist(artistId));
    displayArtistPopularTracks(await getArtistPopularTracks(artistId));
    clearDisplay();
    artistMain.classList.add("active");
}

// Artistin perus tiedot näkyviin
const displayArtist = (artist) => {
    mainDiv.style.backgroundImage = `url(${artist.images[0].url})`;
    let artistFollowers = artist.followers.total.toLocaleString("FI");
    artistTop.innerHTML = `
    <h1>${artist.name}</h1>
    <p>${artistFollowers} seuraajaa</p>
    `;

    const bgImage = new Image();
    bgImage.src = artist.images[0].url;
    bgImage.setAttribute("crossorigin", "anonymous");
    getAverageRgbOfImg(bgImage, [artistPopularTracks.id]);
}

// Artistin 10 suosituinta kappaletta näkyviin
const displayArtistPopularTracks = (artistPopularTracks) => {
    artistPopularTracksInner.innerHTML = "";
    artistPopularTracks.forEach((track, index) => {
        let trackDuration = (((track.duration_ms/1000)/60).toFixed(2)).toString().split(".");
        trackDuration = getTrackDurationMin(trackDuration);
        let html = `
        <div class="track-result">
            <div class="track-result-left">
                <p class="track-number">${index+1}</p>
                <img src="${track.album.images[2].url}" alt="track-result-img">
                <div class="track-result-name-and-artist">
                    <p class="track-name" onclick="_mainJs.showTrack('${track.id}')">${track.name}</p>
                </div>
            </div>
            <p>${trackDuration}</p>
        </div>
        `;
        artistPopularTracksInner.innerHTML += html;
    });
}