// Toivo Lindholm 2023
// Haetaan tiedot spotify web api:sta
// Sekä perustoiminnot
import { getToken, token } from "./auth.js";
import { showSearch, searchInput } from "./search.js";
import { showArtist, mainDiv } from "./artist.js";
import { showAlbum } from "./album.js";
import { showTrack } from "./track.js";

// Haku nappi
const showSearchBtn = document.querySelector("#show-search-icon");
showSearchBtn.addEventListener("click", () => {
    // Haku näkyviin
    showSearch();
});
// Haetaan token
getToken();
// Kun sivu ladataan haku tulee näkyville
showSearch();

export function clearDisplay(){
    mainDiv.scrollTo(0, 0);
    let element = document.querySelector(".active");
    if(element){
        element.classList.remove("active");
    }
};

// Haetaan artistin tiedot
export async function getArtist(artistId){
    const result = await fetch(`https://api.spotify.com/v1/artists/${artistId}`, {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    });
    const artistData = await result.json();
    return artistData;
}
// Haetaan artistin top 10 suosituinta kappaletta
export async function getArtistPopularTracks(artistId){
    const result = await fetch(`https://api.spotify.com/v1/artists/${artistId}/top-tracks?country=FI&access_token=${token}`);
    const artistTopTracks = await result.json();
    return artistTopTracks.tracks;
}

// Haetaan kappaleen tiedot
export async function getTrack(trackId){
    const result = await fetch(`https://api.spotify.com/v1/tracks/${trackId}`, {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    });
    const trackData = await result.json();
    return trackData;
}

// Haetaan kappaleen sanoitukset
export async function getTrackLyrics(trackId){
    const result = await fetch(`https://spotify-lyric-api.herokuapp.com/?trackid=${trackId}`);
    const lyricsData = await result.json();
    return lyricsData;
}

// Haetaan albumin tiedot
export async function getAlbum(albumId){
    const result = await fetch(`https://api.spotify.com/v1/albums/${albumId}`, {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    });
    const albumData = await result.json();
    return albumData;
}

window._mainJs = { showArtist, showAlbum, showTrack };