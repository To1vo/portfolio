// Toivo Lindholm 2023
// Albumin tiedot näkyville
import { clearDisplay, getAlbum, getArtist } from "./main.js";
import { getAverageRgbOfImg } from "./avgRgb.js"
import { mainDiv } from "./artist.js";
import { getTrackDurationMin } from "./track.js";

const albumMain = document.querySelector("[data-album]");
const albumTop = document.querySelector("[data-album-top]");
const albumTrackResults = document.querySelector("[data-album-track-results]");

export async function showAlbum(albumId){
    displayAlbum(await getAlbum(albumId));
    clearDisplay();
    mainDiv.style = "";
    albumMain.classList.add("active");
}

const displayAlbum = async (album) => {
    const albumArtist = await getArtist(album.artists[0].id);
    const albumReleaseDate = album.release_date.split("-");
    const albumDuration = getAlbumDuration(album.tracks.items);
    albumTop.innerHTML = `
    <img id="album-image" src="${album.images[1].url}" alt="album-img" crossorigin="anonymous">
    <div class="base-info">
        <p>Albumi</p>
        <h1>${album.name}</h1>
        <div class="info">
            <img src="${albumArtist.images[2].url}" alt="track-artist-img">
            <p>
                <span class="artist-link" onclick="_mainJs.showArtist('${album.artists[0].id}')">${album.artists[0].name}</span>
                &nbsp•&nbsp;
                <span>${albumReleaseDate[0]}</span>
                &nbsp;•&nbsp;
                <span>${album.total_tracks} kappaletta,<span id="album-duration"> ${albumDuration}</span></span>
            </p>
        </div>
    </div>
    `;

    const displayAlbumTracks = () => {
        const tracks = album.tracks.items;
        albumTrackResults.innerHTML = `
        <div id="album-tracks-top-bottom" class="album-tracks-top-bottom" data-album-tracks-top-bottom></div>
        <div class="album-tracks-top">
        <div>
            <p class="track-number">#</p>
            <p>Nimi</p>
        </div>
        <svg xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 -960 960 960" width="20"><path fill="#b3b3b3" d="m627-287 45-45-159-160v-201h-60v225l174 181ZM480-80q-82 0-155-31.5t-127.5-86Q143-252 111.5-325T80-480q0-82 31.5-155t86-127.5Q252-817 325-848.5T480-880q82 0 155 31.5t127.5 86Q817-708 848.5-635T880-480q0 82-31.5 155t-86 127.5Q708-143 635-111.5T480-80Zm0-400Zm0 340q140 0 240-100t100-240q0-140-100-240T480-820q-140 0-240 100T140-480q0 140 100 240t240 100Z"/></svg>
        </div>
        `;
        tracks.forEach(track => {
            let trackDurationMin = (((track.duration_ms / 1000)/60).toFixed(2)).toString().split(".");
            trackDurationMin = getTrackDurationMin(trackDurationMin);
            let html = `
            <div class="track-result">
            <div class="track-result-left">
                <p class="track-number">${track.track_number}</p>
                <div class="track-result-name-and-artist">
                    <p class="track-name" onclick="_mainJs.showTrack('${track.id}')">${track.name}</p>
                    <p class="artist-link" onclick="_mainJs.showAlbum('${track.artists[0].id}')">${track.artists[0].name}</p>
                </div>
            </div>
            <p>${trackDurationMin}</p>
            </div>
            `;
            albumTrackResults.innerHTML += html;
        });
    }
    displayAlbumTracks();

    const albumImg = document.querySelector("#album-image");
    const albumTracksTopBottom = document.querySelector("[data-album-tracks-top-bottom]");
    getAverageRgbOfImg(albumImg, [albumTop.id, albumTracksTopBottom.id]);
}

//Muutetan albumin pituus minuuteiksi ja näytetään minuutit ja sekunnit erikseen
const getAlbumDuration = (tracks) => {
    const totalDuration = tracks.reduce((total, track) => total + track.duration_ms, 0);
    let totalDurationSeparated = (((totalDuration/1000)/60).toFixed(2)).toString().split(".");
    let durationMin = parseInt(totalDurationSeparated[0]);
    const durationSec = parseFloat(totalDurationSeparated[1]/100);
    durationSec > 0.59 ? durationMin += (durationSec/0.6) : durationMin += durationSec;
    totalDurationSeparated = durationMin.toFixed(2).toString().split(".")

    const completeDuration = `${totalDurationSeparated[0]} min ${totalDurationSeparated[1]} s`;
    return completeDuration;
};