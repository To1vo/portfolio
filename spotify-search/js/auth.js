// Toivo Lindholm 2023 
// Authorization
const clientId = "";
const clientSecret = "";
export let token;

// Haetaan token
export async function getToken(){
    const result = await fetch('https://accounts.spotify.com/api/token', 
    {
        method: 'POST',
        headers: {
            'Content-Type' : 'application/x-www-form-urlencoded',
            'Authorization' : 'Basic ' + btoa(clientId + ':' + clientSecret)
        },
        body: 'grant_type=client_credentials'
    });

    const data = await result.json();
    token = data.access_token;
}