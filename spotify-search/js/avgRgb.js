// Toivo Lindholm 2023
// Selvitetään albumin/artistin kuvan pääväri canvasin avulla

export const getAverageRgbOfImg = (imgsrc, elementsId) => {
    const canvas = document.createElement("canvas");
    const ctx = canvas.getContext("2d");
    imgsrc.onload = () => {
        ctx.drawImage(imgsrc, 0, 0, 1, 1);
        const rgbValues = ctx.getImageData(0, 0, 1, 1).data.slice(0, 3);
        const rgbColor = `rgb(${rgbValues[0]}, ${rgbValues[1]}, ${rgbValues[2]})`;
        elementsId.forEach(elementId => {
            document.querySelector(`#${elementId}`).style.backgroundColor = rgbColor;
        }
        );
    }
}