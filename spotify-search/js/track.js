// Toivo Lindholm 2023
// Kappaleen tiedot ja sanoitukset näkyville
import { clearDisplay, getTrack, getTrackLyrics, getArtist } from "./main.js"
import { getAverageRgbOfImg } from "./avgRgb.js"
import { mainDiv } from "./artist.js";

const trackMain = document.querySelector("[data-track]");
const trackTop = document.querySelector("[data-track-top]");
const trackLyricsTop = document.querySelector("[data-track-lyrics-top]");
const trackLyricsInner = document.querySelector("[data-track-lyrics-inner]");
const trackArtistInner = document.querySelector("[data-track-artist-inner]");

export async function showTrack(trackId){
    displayTrack(await getTrack(trackId));
    displayTrackLyrics(await getTrackLyrics(trackId));
    clearDisplay();
    mainDiv.style = "";
    trackMain.classList.add("active");
}

//Kappaleen tiedot näkyville
const displayTrack = async (track) => {
    //Haetaan kappaleen artistin kuva
    const trackArtist = await getArtist(track.artists[0].id);
    let trackDurationMin = (((track.duration_ms/1000)/60)).toFixed(2).toString().split(".");
    trackDurationMin = getTrackDurationMin(trackDurationMin);
    const albumReleaseYear = track.album.release_date.split("-"); 
    trackTop.innerHTML = `
    <img id="track-album-img" src="${track.album.images[1].url}" crossorigin="anonymous">
    <div class="base-info">
    <p>Kappale</p>
    <h1>${track.name}</h1>
    <div class="info">
    <img src="${trackArtist.images[2].url}" alt="track-artist-img">
    <p>
    <span class="artist-link" onclick="_mainJs.showArtist('${track.artists[0].id}')">${track.artists[0].name}</span>
    &nbsp•&nbsp;
    <span class="album-link" onclick="_mainJs.showAlbum('${track.album.id}')">${track.album.name}</span>
    &nbsp•&nbsp;
    <span>${albumReleaseYear[0]}</span>
    &nbsp;•&nbsp;
    <span>${trackDurationMin}</span>
    </p>
    </div>
    </div>
    `;
    trackArtistInner.setAttribute("onclick", `_mainJs.showArtist('${track.artists[0].id}')`);
    trackArtistInner.innerHTML = `
        <img src="${trackArtist.images[1].url}" alt="track-artist-img">
        <div>
        <p>Artisti</p>
        <p class="artist-link">${track.artists[0].name}</p>
        </div>
        `;
        const trackTopBottom = document.querySelector("[data-track-top-bottom]");
        const trackAlbumImg = document.querySelector("#track-album-img");
        getAverageRgbOfImg(trackAlbumImg, [trackTopBottom.id, trackLyricsTop.id]);
    }
    
//Muutetan kappaleen pituus minuuteiksi
export const getTrackDurationMin = (trackDuration) => {
    let trackDurationMin = parseInt(trackDuration[0])
    const trackDurationSec = parseFloat(trackDuration[1]/100);
    trackDurationSec > 0.59 ? trackDurationMin += (trackDurationSec/0.6) : trackDurationMin += trackDurationSec;
    trackDurationMin = trackDurationMin.toFixed(2).toString().replace(".", ":");
    return trackDurationMin;
}
    
const displayTrackLyrics = (trackLyrics) => {
    const lyrics = trackLyrics.lines;
    trackLyricsInner.innerHTML = "";
    lyrics.forEach(line => {
        trackLyricsInner.innerHTML += `
        <p>${line.words}</p>
    `;
});
}