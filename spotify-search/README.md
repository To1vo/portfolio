# Search app for Spotify

HTML/CSS, JavaScript<br>
[Spotify Web API](https://developer.spotify.com/documentation/web-api)<br>
[Spotify track lyrics](https://github.com/akashrchandran/spotify-lyrics-api)<br>
Sovellus, jossa käyttäjä voi hakea kappaleita, artisteja ja albumeita.<br>
Painamalla esim kappaletta tulee näkyviin tietoja kappaleesta + sanoitukset, sitten painamalla vaikka kappaleen artistia tulee kyseisen artistin tiedot näkyviin.<br> Projektin idea on myös visuaalisesti näyttää aika samalta kuin Spotify.<br>
Käyttääksesi tätä sinun pitää laittaa oma clientId ja clientSecret auth.js tiedostoon.

## Tällä hetkellä toimii
- Kappaleen, albumin ja artistin haku
- Kappaleen, albumin ja artistin tietojen katselu
- Kappaleen sanoitukset 
- Kappale, albumi ja artisti linkit
- Haku sivulle palaaminen
- Tausta värit kuvan päävärin perusteella
- Visuaalisesti valmis
- Tyhjän hakutuloksen ja haussa tapahtuvien virheiden tarkastus

## Tällä hetkellä kesken
- Jos Kappaleelle ei löydy sanoituksia?
- Muiden pienten bugien korjailu

## Uusia ominaisuuksia?

