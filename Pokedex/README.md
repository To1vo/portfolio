# Pokedex

HTML/CSS, JavaScript

Ohjelma hakee pokemonien tiedot [PokeAPI:sta](https://pokeapi.co/).<br>
Käyttäjä voi tarkastella eri pokemonien tietoja.<br>
Ulkoasu jäljittelee oikeaa pokedexia.<br>
[Demo](https://to1vo.gitlab.io/toivo-lindholm/demos/pokedex/)
