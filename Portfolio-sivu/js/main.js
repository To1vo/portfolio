// Toivo Lindholm 2023
const footerText = document.querySelector("#footer-text");
const navToggleOn = document.querySelector(".nav-toggle-on-icon");
const navToggleOff = document.querySelector(".nav-toggle-off");
const mainNavRes = document.querySelector(".main-nav-res");
const links = document.querySelectorAll(".main-nav ul a, .intro-skills-icons a, .about-content a");
const links2 = document.querySelectorAll(".main-nav-res a");
const sliders = document.querySelectorAll(".slider");

const year = new Date().getFullYear();

footerText.innerHTML = "© Toivo Lindholm "+year;

function navToggle(){
    mainNavRes.classList.toggle("main-nav-res-active");
}  

navToggleOn.addEventListener("click", navToggle);
navToggleOff.addEventListener("click", navToggle);
links.forEach(link => {
    link.addEventListener("click", (e)=>{
        e.preventDefault();
        let element = document.querySelector(link.hash);
        if(link.hash == "#about"){
            element.scrollIntoView({block: "center", behavior: "smooth"});
        } else {
            element.scrollIntoView({block: "start", behavior: "smooth"});
        }
    });
});
links2.forEach(link => {
    link.addEventListener("click", (e)=>{
        e.preventDefault();
        let element = document.querySelector(link.hash);
        if(link.hash == "#about"){
            element.scrollIntoView({block: "center", behavior: "smooth"});
        } else {
            element.scrollIntoView({block: "start", behavior: "smooth"});
        }
        navToggle();
    });
});

function sliderNextImg(){
    let newIndex;
    sliders.forEach(slider => {
        let sliderImages = Array.from(document.querySelectorAll(`#${slider.id} img`));

        let currentActiveImg = document.querySelector(`#${slider.id} img.active`);
        currentActiveImg.classList.remove("active");

        let currentActiveImgIndex = sliderImages.indexOf(currentActiveImg);
        if(currentActiveImgIndex == (sliderImages.length-1)){
            newIndex = 0;
        } else {
            newIndex = currentActiveImgIndex+1;
        }

        sliderImages[newIndex].classList.add("active");
    });
}

setInterval(() => {
    sliderNextImg();
}, 3000);