# To-Do App

HTML/CSS, JavaScript<br>
[Demo](https://to1vo.gitlab.io/toivo-lindholm/demos/to-do2/)

## Ominaisuudet
- Käyttäjä voi lisätä ja poistaa tehtävän sekä muokata sitä
- Käyttäjä voi merkitä tehtävän tehdyksi
- Käyttäjä voi erikseen selata tehtyjä, tekemättömiä sekä myös kaikkia tehtäviä
- Ohjelma tallentaa tehtävät selaimen localstorageen

