# Weather App

HTML/CSS, JavaScript

Ohjelma hakee säätiedot [OpenWeatherMap API:sta](https://openweathermap.org/api)

## Ominaisuudet
- Käyttäjä voi hakea tietyn kaupungin/maan säätietoja
- Ohjelma näyttää:
    - Säähän sopivan kuvan
    - Lämpötilan
    - Tuulen nopeuden
    - Kosteuden
