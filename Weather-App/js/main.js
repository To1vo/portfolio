// Toivo Lindholm 2023
const apiKey = "";
const apiUrl = "https://api.openweathermap.org/data/2.5/weather?units=metric&q=";

//Elements
const cityName = document.querySelector(".city");
const temp = document.querySelector(".temp");
const humidity = document.querySelector(".humidity");
const wind = document.querySelector(".wind");
const searchBox = document.querySelector(".search input");
const searchButton = document.querySelector(".search button");
const weatherIcon = document.querySelector(".weather-icon");
const weatherDiv = document.querySelector(".weather");
const errorDiv = document.querySelector(".error");



async function checkWeather(city){
    const response = await fetch(apiUrl + city + `&appid=${apiKey}`);

    //Jos kaupungin nimi on virheellinen
    if(response.status == 404){
        errorDiv.style.display = "block";
        weatherDiv.style.display = "none";
        cloudIcon.style.display = "none";
    } else {
        let data = await response.json();
    
        // console.log(data);
        
        cityName.innerHTML = data.name;
        temp.innerHTML = Math.round(data.main.temp) + "°c";
        humidity.innerHTML = data.main.humidity + "%";
        wind.innerHTML = data.wind.speed + " km/h";
    
        switch(data.weather[0].main){
            case "Clouds":
                weatherIcon.src = "img/clouds.png";
                break;
            case "Clear":
                weatherIcon.src = "img/clear.png";
                break;
            case "Rain":
                weatherIcon.src = "img/rain.png";
                break;
            case "Drizzle":
                weatherIcon.src = "img/drizzle.png";
                break;
            case "Mist":
                weatherIcon.src = "img/mist.png";
                break;
        }
    
        errorDiv.style.display = "none";
        weatherDiv.style.display = "block";
    }
}

searchButton.addEventListener("click", ()=>{
    checkWeather(searchBox.value);
})
