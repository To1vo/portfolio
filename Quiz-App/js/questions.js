export let questions = [
    {
        "name": "Ramones",
        "answered": false,
        "imgSrc": "01"
    },
    {
        "name": "Led Zeppelin",
        "answered": false,
        "imgSrc": "02"
    },
    {
        "name": "Metallica",
        "answered": false,
        "imgSrc": "03"
    },
    {
        "name": "Sex Pistols",
        "answered": false,
        "imgSrc": "04"
    },
    {
        "name": "The Sugarcubes",
        "answered": false,
        "imgSrc": "05"
    },
    {
        "name": "The Beatles",
        "answered": false,
        "imgSrc": "06"
    },
    {
        "name": "AC/DC",
        "answered": false,
        "imgSrc": "07"
    },
    {
        "name": "Red Hot Chili Peppers",
        "answered": false,
        "imgSrc": "08"
    },
    {
        "name": "Nirvana",
        "answered": false,
        "imgSrc": "09"
    },
    {
        "name": "The Kinks",
        "answered": false,
        "imgSrc": "10"
    },
    {
        "name": "Alice in Chains",
        "answered": false,
        "imgSrc": "11"
    },
    {
        "name": "The Rolling Stones",
        "answered": false,
        "imgSrc": "12"
    }
];