// Toivo Lindholm
import { questions } from "./questions.js";

const startDiv = document.querySelector("[data-start]");
const gameDiv = document.querySelector("[data-game]");
const resultDiv = document.querySelector("[data-result]");
const startBtn = document.querySelector("[data-startBtn]");
const resetBtn = document.querySelector("[data-resetBtn]");
const questionCount = document.querySelector("[data-question-count]");
const answers = document.querySelector("[data-answers]");
const bandPicture = document.querySelector("[data-band-picture]");
const finalScore = document.querySelector("[data-final-score]");

const maxQuestions = 10;
let questionCounter = 0;
let correct = 0;
let currentQuestionAnswerIndex;
let notAnsweredQuestions;

// Uusi kysymys
const newQuestion = () => {
    questionCounter += 1;
    questionCount.textContent = `${questionCounter}/10`;
    // Kysymykset joihin ei ole vastattu
    notAnsweredQuestions = questions.filter(question => question.answered == false);
    // Nykyisen kysymyksen index
    currentQuestionAnswerIndex = Math.floor(Math.random() * notAnsweredQuestions.length);
    // Vastaus vaihtoehdot josta ei löydy oikeaa vastausta
    let otherAnswers = questions.filter(question => question.name != notAnsweredQuestions[currentQuestionAnswerIndex].name);
    showAnswers(currentQuestionAnswerIndex, otherAnswers, notAnsweredQuestions);
}

// Näytetään vastaukset
const showAnswers = (currentQuestionAnswerIndex, otherAnswers, notAnsweredQuestions) => {
    answers.innerHTML = "";
    let currentQuestionAnswerIndexPos = Math.floor(Math.random() * 4);
    let answerHtml = "";
    bandPicture.src = `img/${notAnsweredQuestions[currentQuestionAnswerIndex].imgSrc}.png`;
    for(let i=0;i<4;i++){
        if(i == currentQuestionAnswerIndexPos){
            answerHtml = `<div class="answer" onclick="_mainJs.checkAnswer(this.textContent)">${notAnsweredQuestions[currentQuestionAnswerIndex].name}</div>`;
        } else {
            let randomIndex = Math.floor(Math.random() * otherAnswers.length);
            answerHtml = `<div class="answer" onclick="_mainJs.checkAnswer(this.textContent)">${otherAnswers[randomIndex].name}</div>`;
            otherAnswers.splice(randomIndex, 1);
        }
        answers.innerHTML += answerHtml;
    }
}

// Katsotaan onko vastaus oikein
const checkAnswer = (answer) => {
    if(answer.toLowerCase() == notAnsweredQuestions[currentQuestionAnswerIndex].name.toLowerCase()){
            correct += 1;
        }
        questions[questions.indexOf(notAnsweredQuestions[currentQuestionAnswerIndex])].answered = true;
    if(questionCounter == maxQuestions){
            gameDiv.classList.remove("active");
            resultDiv.classList.add("active");
            finalScore.textContent = `Your score: ${correct}/${maxQuestions}`;
    } else {
            newQuestion();
        }
}

// Resetoidaan peli alku tilaan
const reset = () => {
    questionCounter = 0;
    correct = 0;
    currentQuestionAnswerIndex;
    questions.forEach(question => question.answered = false);
    resultDiv.classList.remove("active");
    gameDiv.classList.add("active");
    newQuestion();
}

startBtn.addEventListener("click", () => {
    startDiv.classList.remove("active");
    gameDiv.classList.add("active");
    newQuestion();
});
resetBtn.addEventListener("click", reset);

window._mainJs = {checkAnswer};