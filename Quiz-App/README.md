# Quiz-app

HTML/CSS, JavaScript<br>
Arvaa bändin nimi kuvan perusteella, 10 kysymystä.<br>
[Demo](https://to1vo.gitlab.io/toivo-lindholm/demos/band-quiz/)

## Ominaisuudet
- Aloita peli
- Ohjelma valitsee 12 kysymyksestä randomilla 10
- Valitset aina neljästä vaihtoehdosta mielestäsi oikean
- Tulos tulee näkyville ja voit aloittaa pelin uudestaan

