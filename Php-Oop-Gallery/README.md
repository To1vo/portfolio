# PHP OOP kuva galleria

HTML, CSS/Bootstrap, PHP, SQL, MySQL <br>
Sain [Pinterestistä](https://fi.pinterest.com/) idean tähän PHP:llä toteutettuun kuva galleriaan. Käyttäjien ja kuvien tiedot tallennetaan MySQL tietokantaan. Kuvat itsessään tallentuvat paikallisesti kansioon.

## Ominaisuudet
- Kuvien katselu sekä haku
- Käyttäjien profiilien katselu
- Uuden käyttäjän luominen
- Sisään sekä ulos kirjautuminen

## Kirjautuneena
- Kuvan lisääminen
- Itse lisätyiden kuvien muokkaaminen
    - Kuvan nimi
    - Kuvaus
- Oman profiilin muokkaus
- Muiden kuvien lataaminen
